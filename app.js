import { Record, Tuple } from "@bloomberg/record-tuple-polyfill";
import express from 'express';
const app = express();
const port = 3000;


function visilog(inputString) {
  // Заміна ведучих пробілів на крапки
  const stringWithMiddots = inputString.replace(/^ +/gm, (match) => {
    return '·'.repeat(match.length);
  });

  // Заміна символів табуляції на дефіси
  const finalString = stringWithMiddots.replace(/\t/g, '-');

  return finalString;
}


// Set the view engine to EJS
app.set('view engine', 'ejs');

const record = Record({ a: 1, b: 2, c: 3 });
const array = [1,2,3];

// Define your data
const data = {
  name: 'Аршулік Леонід',
  specialization: 'Гейм-дизайн',
  certification: {
    title: 'Unity User: Programmer',
    link: 'https://unity.com/products/unity-certifications/user-programmer',
  },
  explanation:
    'Я обрав цю сертифікацію, оскільки вона підтверджує мою експертність у програмуванні для розробки ігор на популярній платформі Unity та відкриває нові можливості для кар\'єрного росту у цій галузі, окрім цього сертифікацію також проводить компанія-розробник Unity, що, на мою думку, підвищує її цінність в галузі.',
  isRecord: Record.isRecord(record),
  tupleFromArray: Tuple.from(array),
};

// Define a route to render the information
app.get('/', (req, res) => {
  res.render('index', { data });
});

// Start the server
app.listen(port, () => {
  console.log(`Server is running on http://localhost:${port}`);
});
